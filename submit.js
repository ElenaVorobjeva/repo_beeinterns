document.forms.myForm.onsubmit = function(event) {
    event.preventDefault();
    
    let formData = new FormData(myForm);
    formData.append('name', 'newField');
    formData.append('value', 'customValue');
    formData.delete('hiddenInput');

    fetch(myForm.getAttribute('action'), {
        method: "POST",
        mode: 'no-cors',
        headers: {
            "Content-Type": "form/multipart"
        },
        body: formData
    });

    if(document.querySelector('.success-responce-block')) document.querySelector('.success-responce-block').remove();
    document.forms.myForm.append(createNewElement('span', 'success-responce-block', 'Форма отправлена!'));
    
}

function createNewElement(tag, className, content) {
    let elem = document.createElement(tag);
    elem.classList.add(className);
    elem.innerHTML = content;
    return elem;
}
